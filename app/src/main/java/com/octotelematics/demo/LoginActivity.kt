package com.octotelematics.demo

import android.Manifest
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.octo.core.DDSDK
import com.octo.core.distracted_driving.DDDistraction
import kotlinx.android.synthetic.main.activity_login.*
import java.util.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        DemoApplication.currentActivity = this

        auth.setOnClickListener {
            // LOLLIPOP didn't required explicit permission acceptation
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                val permissions = ArrayList<String>()
                permissions.add(Manifest.permission.ACCESS_COARSE_LOCATION)
                permissions.add(Manifest.permission.ACCESS_FINE_LOCATION)

                // from Q onward this additional permission is required
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                    permissions.add(Manifest.permission.ACTIVITY_RECOGNITION)
                }

                // from Q onward also this additional permission is required but only in Q
                // this can be requested jointly with the others
                if (Build.VERSION.SDK_INT == Build.VERSION_CODES.Q) {
                    permissions.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION)
                }

                // if you want to track also CALL distractions:
                permissions.add(Manifest.permission.READ_PHONE_STATE)
                requestPermissions(permissions.toTypedArray(), 500)
            } else {
                authAndStart()
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        // when a permission is granted, the result value is 0. So we are comparing the results
        // with an empty array [0, 0, 0, ...] of the same length of the permission array
        if (grantResults.contentEquals(IntArray(permissions.size))) {

            // R requires this additional permission to be asked separately
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R && requestCode == 500) {
                requestPermissions(arrayOf(Manifest.permission.ACCESS_BACKGROUND_LOCATION), 250)
            } else {
                authAndStart()
            }
        }
    }

    private fun authAndStart() {

        Toast.makeText(this, "Logging in...", Toast.LENGTH_LONG).show()

        DDSDK.register(
            "INSERT DEVICE ID",
            false,
            "INSERT REGISTRATION URL",
            "INSERT REGISTRATION CODE",
            "INSERT AUTHORIZATION URL"
        ) {
            if (it) {
                // enabling all distractions
                DDDistraction.setup(this, DDDistraction.ALL)

                finish()
                startActivity(Intent(this, MainActivity::class.java))
            } else
                Toast.makeText(this, "SDK register ERROR", Toast.LENGTH_LONG).show()
        }
    }
}
