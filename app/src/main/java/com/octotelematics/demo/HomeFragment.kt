package com.octotelematics.demo

import android.location.Location
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.octo.core.trip.DDTripDetector
import com.octo.core.trip.TripHandler
import kotlinx.android.synthetic.main.fragment_home.*
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment(), TripHandler, AutomaticTrackingChanged {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        tracking.isChecked = DDTripDetector.isTripCreationAutomatic()
        tracking.setOnCheckedChangeListener { buttonView, isChecked ->
            DDTripDetector.setTripCreationAutomatic(isChecked)
            if (isChecked) {
                DemoApplication.automaticTrackingChangedListeners.forEach {
                    it.onAutomaticTrackingEnabled()
                }
            } else {
                DemoApplication.automaticTrackingChangedListeners.forEach {
                    it.onAutomaticTrackingDisabled()
                }
            }
        }

        enable.isEnabled = !DDTripDetector.isEnabled()
        disable.isEnabled = DDTripDetector.isEnabled()

        updateTripDetectorStatus()

        start.isEnabled = !DDTripDetector.isTripOnGoing()
        stop.isEnabled = DDTripDetector.isTripOnGoing()

        enable.setOnClickListener {
            DDTripDetector.enable()
            updateTripDetectorStatus()
            enable.isEnabled = false
            disable.isEnabled = true
        }

        disable.setOnClickListener {
            DDTripDetector.disable()
            updateTripDetectorStatus()
            enable.isEnabled = true
            disable.isEnabled = false
        }

        start.setOnClickListener {
            DDTripDetector.startTrip()
        }

        stop.setOnClickListener {
            DDTripDetector.stopTrip()
        }

        if (DDTripDetector.isTripCreationAutomatic()) {
            onAutomaticTrackingEnabled()
        } else {
            onAutomaticTrackingDisabled()
        }

        DemoApplication.automaticTrackingChangedListeners.add(this)

        in_trip = DDTripDetector.isTripOnGoing()
        updateStatus()
        DDTripDetector.registerTripHandler(this)
    }

    private fun updateTripDetectorStatus() {
        val state = if (DDTripDetector.isEnabled()) "Enabled" else "Disabled"
        try {
            detector.text = "TripDetector status: ${state}"
        } catch (e: java.lang.RuntimeException) {
        }
    }

    override fun onResume() {
        super.onResume()

        if (DDTripDetector.isTripCreationAutomatic()) {
            onAutomaticTrackingEnabled()
        } else {
            onAutomaticTrackingDisabled()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        DemoApplication.automaticTrackingChangedListeners.remove(this)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment HomeFragment.
         */
        @JvmStatic
        fun newInstance() = HomeFragment()
    }

    override fun onLocationChanged(location: Location) {
        last_location = location
        updateStatus()
    }

    override fun onTripStarted() {
        in_trip = true
        updateStatus()
    }

    override fun onTripFinished() {
        in_trip = false
        updateStatus()
    }

    private fun updateStatus() {
        if (status != null) {
            try {
                status.text = "Trip Status: ${getTripStatus()}\nLast Location: ${getLastLocation()}"
            } catch (e: RuntimeException) {
            }
        }
    }

    private var in_trip = false

    private fun getTripStatus(): String {
        start.isEnabled = !in_trip
        stop.isEnabled = in_trip
        if (in_trip) {
            return "TRIP"
        } else {
            return "IDLE"
        }
    }

    private var last_location = Location("")

    private fun getLastLocation(): String {
        val date = SimpleDateFormat.getDateTimeInstance().format(Date(last_location.time))
        return "(${last_location.latitude}, ${last_location.longitude}) - ${date}"
    }

    override fun onAutomaticTrackingEnabled() {
        start.visibility = View.INVISIBLE
        stop.visibility = View.INVISIBLE
        enable.visibility = View.VISIBLE
        disable.visibility = View.VISIBLE
    }

    override fun onAutomaticTrackingDisabled() {
        start.visibility = View.VISIBLE
        stop.visibility = View.VISIBLE
        enable.visibility = View.INVISIBLE
        disable.visibility = View.INVISIBLE
    }
}
