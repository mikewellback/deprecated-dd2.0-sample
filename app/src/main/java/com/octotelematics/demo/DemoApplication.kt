package com.octotelematics.demo

import android.app.Activity
import android.app.Application
import android.app.NotificationChannel
import android.app.PendingIntent
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import com.octo.core.DDSDK
import com.octo.core.distracted_driving.DDDistraction
import com.octo.core.notification.NotificationBuilder

class DemoApplication : Application() {

    companion object {
        val automaticTrackingChangedListeners = mutableListOf<AutomaticTrackingChanged>()
        var currentActivity: Activity? = null
        fun askPermissions(permissions: Array<String>) {
                currentActivity?.requestPermissions(permissions, 500)
        }
    }

    override fun onCreate() {
        super.onCreate()
        val eh = object : DemoEventHandler() {
            override fun askPermissions(permissions: Array<String>) {
                DemoApplication.askPermissions(permissions)
            }
        }
        DDSDK.setup(this, null, eh, object : NotificationBuilder(this@DemoApplication, eh) {
            override fun handleTrip() {
                super.handleTrip()
                showTripStatusNotification(true)
            }

            override fun handleTripStopped(tripId: Long) {
                super.handleTripStopped(tripId)
                showTripStatusNotification(false)
            }
        })
    }

    private fun showTripStatusNotification(inTrip: Boolean) {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as android.app.NotificationManager
        val intent = packageManager.getLaunchIntentForPackage(packageName)
        val pendingIntent = PendingIntent.getActivity(this@DemoApplication, 2, intent, 0)
        val notification = NotificationCompat.Builder(this@DemoApplication, "TRIP STATUS")
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle("Trip status changed")
            .setAutoCancel(false)
            .setPriority(NotificationCompat.PRIORITY_MIN)
            .setContentIntent(pendingIntent)
            .setOngoing(false)
            .setStyle(NotificationCompat.BigTextStyle().bigText(when (inTrip) { true -> "TRIP STARTED"; false -> "TRIP STOPPED" }))
            .build()

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                val channel = NotificationChannel(
                    "TRIP STATUS",
                    "Trip status",
                    android.app.NotificationManager.IMPORTANCE_DEFAULT
                )
                channel.description = "Notifications about trip status change"
                notificationManager.createNotificationChannel(channel)
            }
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        notificationManager.notify((System.currentTimeMillis() % 100000000).toInt(), notification)
    }
}