package com.octotelematics.demo

import android.Manifest
import android.os.Build
import android.util.Log
import com.octo.core.DDEventHandler

abstract class DemoEventHandler : DDEventHandler {
    abstract fun askPermissions(permissions: Array<String>)

    override fun onTripStarted() {
        Log.d("EVENT_HANDLER", "onTripStarted")
    }

    override fun onTripStopped() {
        Log.d("EVENT_HANDLER", "onTripStopped")
    }

    override fun onBatteryLow() {
        Log.d("EVENT_HANDLER", "onBatteryLow")
    }

    override fun onAuthFailed() {
        Log.d("EVENT_HANDLER", "onAuthFailed")
    }

    override fun onMissingLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            askPermissions(arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_BACKGROUND_LOCATION
            ))
        } else {
            askPermissions(arrayOf(
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION
            ))
        }
        Log.d("EVENT_HANDLER", "onMissingLocationPermission")
    }

    override fun onGpsRequired() {
        Log.d("EVENT_HANDLER", "onGpsRequired")
    }

    override fun onMissingActivityRecognitionPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            askPermissions(arrayOf(
                Manifest.permission.ACTIVITY_RECOGNITION
            ))
        }
        Log.d("EVENT_HANDLER", "onMissingActivityRecognitionPermission")
    }

    override fun onMissingPhonePermission() {
        askPermissions(arrayOf(
            Manifest.permission.READ_PHONE_STATE
        ))
        Log.d("EVENT_HANDLER", "onMissingPhonePermission")
    }
}