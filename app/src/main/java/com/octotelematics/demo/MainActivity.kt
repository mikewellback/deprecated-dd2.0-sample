package com.octotelematics.demo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator
import com.octo.core.DDSDK
import com.octo.core.trip.DDTripDetector
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.*

class MainActivity : AppCompatActivity() {

    private lateinit var pagesAdapter: TabsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        DemoApplication.currentActivity = this

        if (!DDSDK.isRegistered()) {
            finish()
            startActivity(Intent(this, LoginActivity::class.java))
        } else {
            DDSDK.start(this)
        }

        pagesAdapter = TabsAdapter(this, tab)
        pages.adapter = pagesAdapter
        TabLayoutMediator(tab, pages) { tab, pos -> tab.text = TabsAdapter.getTitle(pos) }.attach()
    }

    class TabsAdapter(activity: FragmentActivity, private val tabLayout: TabLayout):
        FragmentStateAdapter(activity) {

        companion object {
            fun getTitle(position: Int): String {
                if (position == 0) return "HOME"
                else if (position == 1) return "TAG"
                else return "CONF"
            }
        }

        override fun getItemCount(): Int = 3

        override fun createFragment(position: Int): Fragment {
            if (position == 0) return HomeFragment()
            else if (position == 1) return TagFragment()
            else return ConfFragment()
        }
    }
}
