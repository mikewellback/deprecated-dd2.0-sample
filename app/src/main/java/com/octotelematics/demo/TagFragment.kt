package com.octotelematics.demo

import android.Manifest
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.octo.core.DDSDK
import com.octo.core.tag.*
import com.octo.shared.model.BLE
import com.octotelematics.demo.adapters.TagAdapter
import kotlinx.android.synthetic.main.dialog_addtag.view.*
import kotlinx.android.synthetic.main.fragment_tag.*
import kotlinx.android.synthetic.main.item_tag.view.*
import java.lang.IllegalStateException

/**
 * A simple [Fragment] subclass.
 * Use the [TagFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TagFragment : Fragment(), BleDelegate, BLEScanningHandler, DDBleCalibrationHandler {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tag, container, false)
    }

    private val tagAdapter = TagAdapter()
    private var fotaUrl = "INSERT FOTA URL"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var tagmode = DDBLE.getTagMode()

        mode.isChecked = tagmode == DDBLE.Mode.TAG
        mode.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                tagmode = DDBLE.Mode.TAG
            } else {
                tagmode = DDBLE.Mode.MIX
            }
            DDBLE.setTagMode(tagmode)
        }

        add.setOnClickListener {
            DDBLE.setup(context!!, tagmode, false, fotaUrl, DDBLE.ALL, this)
            if (context != null) {
                var dia: AlertDialog? = null
                val lay = LayoutInflater.from(context!!).inflate(R.layout.dialog_addtag, null)
                lay.button.setOnClickListener {
                    val ble = BLE.fromSerialNumber(lay.text.text.toString())
                    if (ble != null && ble.major >= 0 && ble.minor > 0) {
                        val list = mutableListOf<BLE>()
                        list.addAll(tagAdapter.getList())
                        list.add(ble)
                        tagAdapter.setList(list)
                        dia?.dismiss()
                    } else {
                        lay.text.setError("Wrong serial")
                    }
                }
                dia = AlertDialog.Builder(context!!)
                    .setView(lay)
                    .setTitle("Tag Serial")
                    .show()
            }
        }

        tags.adapter = tagAdapter

        start_scan.setOnClickListener {
            if (tagAdapter.getList().isEmpty()) {
                if (context != null) {
                    Toast.makeText(context, "Add a tag", Toast.LENGTH_LONG).show()
                }
            } else {
                DDBLE.startBleScan(context!!, tagAdapter.getList(), this)
            }
        }

        stop_scan.setOnClickListener {
            DDBLE.stopScan()
        }

        val connectedTags = DDBLE.getStoredTags()

        if (connectedTags.isEmpty()) {
            connectedTag.visibility = View.GONE
            calibrationStatus.visibility = View.GONE
            calibrate.visibility = View.GONE
            fotaCheck.visibility = View.GONE
            fotaStart.visibility = View.GONE
            connect.text = "CONNECT"
        } else {
            DDBLE.setup(context!!, tagmode, false, fotaUrl, DDBLE.ALL, this)
            val ble = connectedTags.first()
            val list = mutableListOf<BLE>()
            list.addAll(tagAdapter.getList())
            list.add(ble)
            tagAdapter.setList(list)
            connectedTag.serial.text = ble.serialNumber
            connectedTag.status.text = "Stored"
            connectedTag.visibility = View.VISIBLE
            calibrationStatus.visibility = View.VISIBLE
            calibrate.visibility = View.VISIBLE
            calibrationStatus.text = "Calibrated: OK"
            fotaCheck.visibility = View.VISIBLE
            fotaStart.visibility = View.VISIBLE
            connect.text = "DISCONNECT"
        }

        connect.setOnClickListener {
            val tag = tagAdapter.getFoundTag()
            if (tag != null) {
                DDBLE.disconnect {
                    connectedTag?.visibility = View.GONE
                    calibrationStatus.visibility = View.GONE
                    calibrationStatus.text = "Calibrated: KO"
                    calibrate.visibility = View.GONE
                    fotaCheck.visibility = View.GONE
                    fotaStart.visibility = View.GONE
                    connect.text = "CONNECT"
                    if (!it) {
                        if (context != null) {
                            Toast.makeText(context, "Connecting...", Toast.LENGTH_LONG).show()
                        }
                        DDBLE.connect(tag, true) {
                            connectedTag?.serial?.text = tag.serialNumber
                            connectedTag?.status?.text = "Connected"
                            connectedTag?.visibility = View.VISIBLE
                            calibrationStatus.visibility = View.VISIBLE
                            calibrate.visibility = View.VISIBLE
                            fotaCheck.visibility = View.VISIBLE
                            fotaStart.visibility = View.VISIBLE
                            connect.text = "DISCONNECT"
                        }
                    }
                }
            } else if (context != null) {
                Toast.makeText(context, "Select a tag", Toast.LENGTH_LONG).show()
            }
        }

        calibrate.setOnClickListener {
            Toast.makeText(context, "Calibrating...", Toast.LENGTH_LONG).show()
            DDBLE.startCalibration(this)
        }

        fotaCheck.setOnClickListener {
            if (DDBLE.getStoredTags().isEmpty()) {
                if (context != null) {
                    Toast.makeText(context, "No tags stored", Toast.LENGTH_LONG).show()
                }
            } else {
                if (context != null) {
                    Toast.makeText(context, "Checking for updates...", Toast.LENGTH_LONG).show()
                }
                DDBLE.checkUpdate {
                    if (!it && context != null) {
                        Toast.makeText(context, "No updates available", Toast.LENGTH_LONG).show()
                    }
                    try {
                        fotaStart.isEnabled = it
                    } catch (e: IllegalStateException) {
                    }
                }
            }
        }

        fotaStart.setOnClickListener {
            DDBLE.startFota()
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment TagFragment.
         */
        @JvmStatic
        fun newInstance() = TagFragment()
    }

    override fun bluetoothPermissionRequired() {
        DemoApplication.askPermissions(arrayOf(
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN
        ))
        Log.d("TAG_FRAGMENT", "bluetoothPermissionRequired")
    }

    override fun bluetoothDisabled() {
        Log.d("TAG_FRAGMENT", "bluetoothDisabled")
    }

    override fun fotaStart() {
        Log.d("TAG_FRAGMENT", "fotaStart")
    }

    override fun fotaEnd(status: Boolean) {
        Log.d("TAG_FRAGMENT", "fotaEnd")
        fotaStart.isEnabled = false
    }

    override fun tagConnectionChanges() {
        Log.d("TAG_FRAGMENT", "tagConnectionChanges")
    }

    override fun fotaStatusChanged(state: DDBleDFUState) {
        Log.d("TAG_FRAGMENT", "fotaStatusChanged: ${state}")
    }

    override fun onTagFound(ble: BLE) {
        if (context != null) {
            Toast.makeText(context, "Tag found: ${ble.serialNumber}", Toast.LENGTH_LONG).show()
        }
        Log.d("TAG_FRAGMENT", "onTagFound: ${ble}")
        tagAdapter.addFoundTag(ble)
    }

    override fun onScanFinish() {
        Log.d("TAG_FRAGMENT", "onScanFinish")
    }

    override fun onBTDisabled() {
        Log.d("TAG_FRAGMENT", "onBTDisabled")
    }

    override fun onMissingBTPermission() {
        DemoApplication.askPermissions(arrayOf(
            Manifest.permission.BLUETOOTH,
            Manifest.permission.BLUETOOTH_ADMIN
        ))
        Log.d("TAG_FRAGMENT", "onMissingBTPermission")
    }

    override fun onMissionLocationPermission() {
        DemoApplication.askPermissions(arrayOf(
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
        ))
        Log.d("TAG_FRAGMENT", "onMissionLocationPermission")
    }

    override fun onAccelerationChanged(x: Float, y: Float, z: Float, pitch: Int, roll: Int) {
    }

    override fun onCalibrationFinished() {
        connectedTag?.status?.text = "Stored"
        calibrationStatus?.text = "Calibrated: OK"
    }

    override fun unsupportedCrashes() {
        Log.d("TAG_FRAGMENT", "unsupportedCrashes")
    }

    override fun updateAvailable(version: String) {
        Log.d("TAG_FRAGMENT", "updateAvailable: $version")
        try {
            fotaStart.isEnabled = true
        } catch (e: IllegalStateException) {}
    }
}
