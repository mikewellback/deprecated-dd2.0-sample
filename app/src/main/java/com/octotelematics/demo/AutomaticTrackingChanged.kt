package com.octotelematics.demo

interface AutomaticTrackingChanged {
    fun onAutomaticTrackingEnabled()
    fun onAutomaticTrackingDisabled()
}