package com.octotelematics.demo.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.octo.shared.model.BLE
import com.octotelematics.demo.R
import kotlinx.android.synthetic.main.item_tag.view.*

class TagAdapter() : RecyclerView.Adapter<TagAdapter.TagViewHolder>() {

    private var selectedTag: BLE? = null
    private var list: List<BLE> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TagViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_tag, parent, false)
        return TagViewHolder(v)
    }

    fun setList(items: List<BLE>) {
        list = items.distinct()
        notifyDataSetChanged()
    }

    fun selectTag(ble: BLE) {
        selectedTag = list.firstOrNull { it == ble }
        selectedTag?.macAddress = ble.macAddress
        notifyDataSetChanged()
    }

    fun addFoundTag(ble: BLE) {
        val foundTag = list.firstOrNull { it == ble }
        foundTag?.macAddress = ble.macAddress
        notifyDataSetChanged()
    }

    fun addItem(item: BLE) {
        if (!this.list.contains(item)) {
            this.list += item
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: TagViewHolder, position: Int) {
        holder.bind(list[position])
    }

    fun getList(): List<BLE> {
        return list
    }

    fun getFoundTag(): BLE? {
        return selectedTag
    }

    inner class TagViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        fun bind(device: BLE) {
            itemView.tag = device
            itemView.serial.text = device.serialNumber
            itemView.setOnClickListener(this)
            if (selectedTag == device) {
                itemView.alpha = 1.0f
                itemView.status.text = "Selected"
            } else if (device.macAddress != null && device.macAddress.isNotBlank()) {
                itemView.alpha = 0.5f
                itemView.status.text = "Found"
            } else {
                itemView.alpha = 0.25f
                itemView.status.text = "Not found"
            }
        }

        override fun onClick(v: View?) {
            val ble = v?.tag as? BLE
            if (ble != null && v.alpha == 0.5f) {
                selectedTag = list.firstOrNull { it == ble }
                notifyDataSetChanged()
            }
        }
    }
}