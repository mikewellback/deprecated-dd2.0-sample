package com.octotelematics.demo

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.octo.core.DDSDK
import com.octo.core.distracted_driving.DDDistraction
import com.octo.core.distracted_driving.DistractionType
import com.octo.core.trip.DDTripDetector
import kotlinx.android.synthetic.main.fragment_conf.*

/**
 * A simple [Fragment] subclass.
 * Use the [ConfFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ConfFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_conf, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val enabledDistractions = DDDistraction.getEnabledDistractions(context!!).toMutableList()

        call_handsfree.isChecked = enabledDistractions.contains(DistractionType.CALL_HANDSFREE)
        call_handsfree.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                enabledDistractions.add(DistractionType.CALL_HANDSFREE)
            } else {
                enabledDistractions.remove(DistractionType.CALL_HANDSFREE)
            }
            DDDistraction.setup(context!!, enabledDistractions)
        }

        call_handheld.isChecked = enabledDistractions.contains(DistractionType.CALL_HANDHELD)
        call_handheld.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                enabledDistractions.add(DistractionType.CALL_HANDHELD)
            } else {
                enabledDistractions.remove(DistractionType.CALL_HANDHELD)
            }
            DDDistraction.setup(context!!, enabledDistractions)
        }

        handling.isChecked = enabledDistractions.contains(DistractionType.HANDLING)
        handling.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                enabledDistractions.add(DistractionType.HANDLING)
            } else {
                enabledDistractions.remove(DistractionType.HANDLING)
            }
            DDDistraction.setup(context!!, enabledDistractions)
        }

        awake.isChecked = enabledDistractions.contains(DistractionType.AWAKE)
        awake.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                enabledDistractions.add(DistractionType.AWAKE)
            } else {
                enabledDistractions.remove(DistractionType.AWAKE)
            }
            DDDistraction.setup(context!!, enabledDistractions)
        }

        labelVersion.text = "SDK Version ${DDSDK.getVersion()}"
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @return A new instance of fragment TagFragment.
         */
        @JvmStatic
        fun newInstance() = ConfFragment()
    }
}
